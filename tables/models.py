from django.db import models


class Task(models.Model):
    title = models.CharField(max_length=200, null=True)
    description = models.CharField(max_length=200, null=True)
    column = models.ForeignKey('Column', on_delete=models.CASCADE)
    project= models.ForeignKey('Project', on_delete=models.CASCADE)


class Column(models.Model):
    title = models.CharField(max_length=200, null=True)
    isDefault = models.BooleanField(null=True)
    color = models.CharField(max_length=50, null=True)
    project = models.ForeignKey('Project', on_delete=models.CASCADE, null=True)

class Project(models.Model):
    title = models.CharField(max_length=200)