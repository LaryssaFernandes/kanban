from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status

from .models import Column, Task, Project
from .serializers import *

from rest_framework.decorators import api_view

@api_view(['GET', 'POST'])
def columns_list(request):
    if request.method == 'GET':
        columns = Column.objects.all()
        columns_serializer = ColumnSerializer(columns, many=True)
        return JsonResponse(columns_serializer.data, safe=False)
        # 'safe=False' for objects serialization
 
    elif request.method == 'POST':
        column_data = JSONParser().parse(request)
        column_serializer = ColumnSerializer(data=column_data)
        if column_serializer.is_valid():
            column_serializer.save()
            return JsonResponse(column_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(column_serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

def column_detail(request, pk):
    """
    Retrieve, update or delete a column by id.
    """
    try: 
        column = Column.objects.get(id=pk) 
    except Column.DoesNotExist: 
        return JsonResponse({'message': 'The column does not exist'}, status=status.HTTP_404_NOT_FOUND) 
 
    if request.method == 'GET': 
        column_serializer = ColumnSerializer(column) 
        return JsonResponse(column_serializer.data) 
 
    elif request.method == 'PUT': 
        column_data = JSONParser().parse(request) 
        column_serializer = ColumnSerializer(column, data=column_data) 
        if column_serializer.is_valid(): 
            column_serializer.save() 
            return JsonResponse(column_serializer.data) 
        return JsonResponse(column_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
 
    elif request.method == 'DELETE': 
        column.delete() 
        return JsonResponse({'message': 'Column was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'POST'])
def tasks_list(request):
    if request.method == 'GET':
        tasks = Task.objects.all()
        tasks_serializer = TaskSerializer(tasks, many=True)
        return JsonResponse(tasks_serializer.data, safe=False)
        # 'safe=False' for objects serialization
 
    elif request.method == 'POST':
        task_data = JSONParser().parse(request)
        tasks_serializer = TaskSerializer(data=task_data)
        if tasks_serializer.is_valid():
            tasks_serializer.save()
            return JsonResponse(tasks_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(tasks_serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

def task_detail(request, pk):
    """
    Retrieve, update or delete a task by id.
    """
    try: 
        task = Task.objects.get(pk=pk) 
    except Task.DoesNotExist: 
        return JsonResponse({'message': 'The task does not exist'}, status=status.HTTP_404_NOT_FOUND) 
 
    if request.method == 'GET': 
        task_serializer = TaskSerializer(task) 
        return JsonResponse(task_serializer.data) 
 
    elif request.method == 'PUT': 
        task_data = JSONParser().parse(request) 
        task_serializer = TaskSerializer(task, data=task_data) 
        if task_serializer.is_valid(): 
            task_serializer.save() 
            return JsonResponse(task_serializer.data) 
        return JsonResponse(task_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
 
    elif request.method == 'DELETE': 
        task.delete() 
        return JsonResponse({'message': 'Task was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'POST'])
def projects_list(request):
    if request.method == 'GET':
        projects = Project.objects.all()
        projects_serializer = ProjectSerializer(projects, many=True)
        return JsonResponse(projects_serializer.data, safe=False)
        # 'safe=False' for objects serialization
 
    elif request.method == 'POST':
        project_data = JSONParser().parse(request)
        project_serializer = ProjectSerializer(data=project_data)
        if project_serializer.is_valid():
            project_serializer.save()
            return JsonResponse(project_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(project_serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

def project_detail(request, id):
    """
    Retrieve, update or delete a project by id.
    """
    try: 
        project = Project.objects.get(pk=pk) 
    except Project.DoesNotExist: 
        return JsonResponse({'message': 'The project does not exist'}, status=status.HTTP_404_NOT_FOUND) 
 
    if request.method == 'GET': 
        project_serializer = ProjectSerializer(project) 
        return JsonResponse(project_serializer.data) 
 
    elif request.method == 'PUT': 
        project_data = JSONParser().parse(request) 
        project_serializer = ProjectSerializer(project, data=project_data) 
        if project_serializer.is_valid(): 
            project_serializer.save() 
            return JsonResponse(project_serializer.data) 
        return JsonResponse(project_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
 
    elif request.method == 'DELETE': 
        project.delete() 
        return JsonResponse({'message': 'Project was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
