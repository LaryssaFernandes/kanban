from rest_framework import serializers
from .models import Column, Task, Project

class ColumnSerializer(serializers.ModelSerializer):

    class Meta:
        model = Column
        fields = ('pk','title', 'color', 'isDefault', 'project')

class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = ('pk','title', 'description', 'column', 'project')

class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = ('pk','title')