Para rodar a aplicação, deve se inicialmente checar se estão instalados (e atualizados) na máquina (rode os seguintes comandos na pasta raiz do projeto):

- NODE: node -v (https://nodejs.org/en/download/);
- DJANGO: python -m django --version (python -m pip install --upgrade pip);
- POSTGREE (https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows).
- REACT.

Caso ambos ja estejam instalados, deve-se seguir as seguintes instruções:

### Rodar o servidor
Antes de irmos pro código, você deve criar um servidor com o nome *kanban* no Postgree (o que pode ser feito de maneira fácil pelo PgAdmin4), que iremos usar para salvar, acessar e alterar os dados necessários para nosso projeto. Depois disso, modifique o seu usuário e senha no arquivo *settings.py* - campos 'USER' e 'PASSWORD', de acordos com o usuário cadastrado no seu Postgree. Depois disso, devemos abrir novamente um terminal de comandos dentro da pasta raiz do projeto, e rodar os seguintes comandos:

- -> python manage.py makemigrations (vai criar as migrations 0001 e 0001, criando e populando as tabelas)
- -> python manage.py migrate (vai executar as migrações, colocando essas informações no servidor criado)
- -> python manage.py runserver (esse comando, por fim, irá colocar nosso servidor pra rodar!)

Dessa forma, o servidor django estará rodando e estamos prontos para o próximo passo.

### Instalar as dependências do node
Primeiramente deve-se abrir um terminal de comandos dentro da pasta *reactapp*, e rodar o seguinte comando:

- -> npm install (esse passo deve demorar um pouco, pois muitas dependências do node serão instaladas para o funcionamento do projeto)

### Iniciando o projeto
Continuar na pasta *reactapp*, e rodar o seguinte comando:

- -> npm start

Após isso, a aplicação pode ser vista no endereço http://localhost:3000/. 