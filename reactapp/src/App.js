import React, { Component } from 'react';
import './App.css';
import Box from "./components/Box"
import SelectProject from './components/Selects/ChangeProject';
import ModalProject from "./components/Modal/newProject";
import ModalColumn from "./components/Modal/newColumn";
import ModalTask from "./components/Modal/newTask";
import ModalEditTask from "./components/Modal/editTask"
import ColumnsService from './services/ColumnsService';
import ProjectsService from './services/ProjectsService';
import Colors from './components/Selects/Colors';
const columnsService = new ColumnsService();
const projectsService = new ProjectsService();

class App extends Component {

  constructor() {
      super();
      this.state = {
          isShowingProject: false,
          isShowingColumn: false,
          isShowingTask: false,
          isShowingEditTask: false,
          columns: [],
          selectColumn: "",
          projectId: 1
      }
      this.getColumns = this.getColumns.bind(this);
      this.columnChange = this.columnChange.bind(this);
      this.getColumns(false); //false indica que não é update e sim create
  }

  columnChange(event) {
    this.setState({
      selectColumn: event.target.value
    });
  }

  getProjects(){
    projectsService.getProjects().then((projects)=> {
      document.getElementById("projects-select").value = projects
    });
  }

  openModalProject = () => {
    this.closeModalColumn();
    this.closeModalTask();
      this.setState({isShowingProject: true});
      document.getElementById("div-modal-project").style.display = "block";
  }

  closeModalProject(onReload) {
    this.setState({isShowingTask: false});

    document.getElementById("div-modal-project").style.display = "none";
    if (onReload) {
      this.getProjects();
      this.getColumns(true);
      window.location.reload();
    }
  }

  openModalColumn = () => {
    this.closeModalProject();
    this.closeModalTask();
    
    this.setState({isShowingColumn: true});

    document.getElementById("div-modal-column").style.display = "block";
  }

  closeModalColumn = (onReload) => {
    this.setState({isShowingColumn: false});

    document.getElementById("div-modal-column").style.display = "none";
    if (onReload) this.getColumns(true);
  }

  openModalTask = () => {
    this.closeModalColumn();
    this.closeModalProject();
    this.setState({isShowingTask: true});

    document.getElementById("div-modal-task").style.display = "block";
  }

  closeModalTask = (onReload) => {
    this.setState({isShowingTask: false});

    document.getElementById("div-modal-task").style.display = "none";
    if (onReload) this.getColumns(true);
  }

  openModalEditTask = () => {
    this.closeModalColumn();
    this.closeModalProject();
    this.setState({isShowingEditTask: true});

    document.getElementById("div-edit-task").style.display = "block";
  }

  closeModalEditTask = (onReload) => {
    this.setState({isShowingEditTask: false});

    document.getElementById("div-edit-task").style.display = "none";
    if (onReload) this.getColumns(true);
  }

  getColumns(update){
    if (update) { //se for true, atualiza o estado de projectId
      this.setState({
        columns: [],
        projectId: parseInt(document.getElementById("select-project").value) || this.state.projectId
      });
    }

    columnsService.getColumns().then((columns)=> {
        this.setState({columns: columns});
    });
  }

  render () {
    return (
      <div className="App">
          <h2 className="title">Kanban</h2>
          <div className="buttons">
            <p>
              Add New Column
              <input type="button" className="button-add" value="+" onClick={this.openModalColumn}></input>
            </p>
            <p>
              Add Task
              <input type="button" className="button-add" value="+" onClick={this.openModalTask}></input>
            </p>
            <p>
              Add New Project
              <input type="button" className="button-add" value="+" onClick={this.openModalProject}></input>
            </p>
          </div>
          <SelectProject updateColumns={this.getColumns}></SelectProject>
          <div className="boxes">
            {
              this.state.columns.map(
                column=> 
                column.isDefault ? 
                  <Box isDefault={column.isDefault} key={column.pk} columnId={column.pk} columnTitle={column.title} colorName={column.color}></Box>
                : column.project === this.state.projectId  ? 
                  <Box updateColumns={this.getColumns} isDefault={column.isDefault} key={column.pk} columnId={column.pk} columnTitle={column.title} colorName={column.color}></Box>
                : ''
              )
            }
          </div>
          <div className="div-modal" id="div-modal-column">
            { this.state.isShowingColumn ? <div onClick={this.closeModalColumn} ></div> : null }
            <ModalColumn
              className="modal"
              show={this.state.isShowingColumn}
              close={() => {this.closeModalColumn(true)}}>
                  <div className="text-left display-inline">
                    <input id="input-modal-column" className="input-modal-column" type="text" placeholder="Title"></input>
                    <Colors></Colors>
                  </div>
            </ModalColumn>
          </div>
          <div className="div-modal" id="div-modal-project">
            { this.state.isShowingProject ? <div onClick={this.closeModalProject} ></div> : null }
            <ModalProject
              className="modal"
              show={this.state.isShowingProject}
              close={() => {this.closeModalProject(true)}}>
                  <input id="input-modal-project" className="input-modal-project" type="text" placeholder="Title"></input>
            </ModalProject>
          </div> 
          <div className="div-modal" id="div-modal-task">
            { this.state.isShowingTask ? <div onClick={this.closeModalTask} ></div> : null }
            <ModalTask
              className="modal"
              show={this.state.isShowingTask}
              close={() => {this.closeModalTask(true)}}>
                  <input id="title-task" className="input-modal-task" type="text" placeholder="Title"></input>
                  <input id="description-task" className="input-modal-task" type="text" placeholder="Description"></input>
            </ModalTask>
          </div> 
          <div className="div-modal" id="div-edit-task">
            { this.state.isShowingEditTask ? <div onClick={this.closeModalEditTask} ></div> : null }
            <ModalEditTask
              className="modal"
              show={this.state.isShowingEditTask}
              close={() => {this.closeModalEditTask(true)}}>
                <div id="projects-select" onChange={this.changeProjects} value={this.props.dados}></div>
                  <p id="id-edit-task" display="none" value=""></p>
                  <p className="text-left">
                    <label className="label-title-task">Title: </label>
                    <input id="title-edit-task" className="input-modal-task text-center" type="text"></input>
                  </p>
                  <p className="text-left">
                    <label className="label-description-task">Description: </label>
                    <input id="description-edit-task" className="input-modal-task text-center" type="text"></input>
                  </p>
                  <p className="text-left">
                  <label className="label-status-task">Status: </label>
                    <select id="select-column" className="input-modal-task text-center" value={this.state.selectColumn} onChange={this.columnChange}>
                    {
                      this.state.columns.map(
                        column=> column.isDefault ? <option key={column.title} value={column.pk}>{column.title}</option> 
                        : column.project === this.state.projectId ? 
                         <option key={column.title} value={column.pk}>{column.title}</option>
                        : ''
                      )
                    }
                    </select>
                  </p>   
            </ModalEditTask>
          </div>
      </div>
    );
  }
}

export default App;
