import React, {Component} from 'react';
import "./Select.css";

export default class Select extends Component{

    constructor(props) {
        super(props);
        this.state = {value: 1};
        this.handleChange = this.handleChange.bind(this);
        this.changeProjects = this.changeProjects.bind(this);
      }

      handleChange(event) {
        this.setState({value: event.target.value});
        document.getElementById("select-project").value = event.target.value;
        this.props.updateColumns(true); //atualiza as colunas de acordo com o projeto
      }

      changeProjects() {
        this.props.dados = document.getElementById("projects-select").value;
      }

    render(){
        return(
            <div id="select-project" className="display-contents">
                <div id="projects-select" onChange={this.changeProjects} value={this.props.dados}></div>
                <select id="select" value={this.state.value} onChange={this.handleChange}>
                    {
                        this.props.dados.map(
                            row=><option key={row.title} value={row.pk}>{row.title}</option>
                        )
                    }
                </select>
            </div>
        );
    }

}