import React, {Component} from 'react';
import "./Colors.css";

export default class Colors extends Component{
    constructor() {
        super();

        this.state = {
            value: '#FF0000',
        }
    }

    handleChange = () => {
        this.setState({
            value: document.getElementById("cores").value
        });
    }

    render(){
        return(
            <div className="div-colors">
                <p className="inline">Choose Color: </p>
                <input type="color" id="cores" name="ArcoIris" list="arcoIris" onChange={this.handleChange} value={this.state.value}/>
                <datalist id="arcoIris">
                    <option value="#FF0000">Vermelho</option>
                    <option value="#FFA500">Laranja</option>
                    <option value="#FFFF00">Amarelo</option>
                    <option value="#008000">Verde</option>
                    <option value="#0000FF">Azul</option>
                    <option value="#4B0082">Indigo</option>
                    <option value="#EE82EE">Violeta</option>
                </datalist>
            </div>
        );
    }
}