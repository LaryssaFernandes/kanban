import React, { Component } from "react";
import "./ChangeProject.css";
import Select from './Select';
import ProjectsService from "../../services/ProjectsService";
const  projectsService  =  new  ProjectsService();

class SelectProject extends Component {

    constructor(props){
        super(props);
        this.state=({
            projects:[]
        });

        this.getProjects();
    }
    
    getProjects(){
        projectsService.getProjects().then((projects)=> {
            this.setState({projects: projects});
        });
    }

    render() {
        return (
        <div className="inline">
            <h3 className="inline">Change Project: </h3>
            <Select updateColumns={this.props.updateColumns} dados={this.state.projects} />
        </div>
        );
    }
}

export default SelectProject;
