import React, { Component } from "react";
import "./Box.css";
import TasksService from '../services/TasksService';
import ColumnService from '../services/ColumnsService';
import Task from './Task';
const tasksService = new TasksService();
const columnService = new ColumnService();

class Box extends Component {

  constructor(props) {
    super(props);

    this.state = {
        tasks: []
    }

    this.getTasks();
    this.deleteColumn = this.deleteColumn.bind(this);

  }

  deleteColumn() {
    if (this.state.tasks.length !== 0) { //checa se existem tasks na coluna a ser deletada
      return window.alert("A coluna possui tasks e por isso não pode ser deletada!");
    }
    columnService.deleteColumn(this.props.columnId).then(() => {
      return this.props.updateColumns(true);
    });
  }

  getTasks(){
    tasksService.getTasks().then((tasks)=> {
        var current_tasks = [];
        tasks.forEach(task => {
          if (task.column === this.props.columnId && task.project.toString() === document.getElementById("select").value)
            current_tasks.push(task);
        });
        this.setState({tasks: current_tasks});
    });
  }

  render() {
    return (    
      <div style={{backgroundColor: this.props.colorName}} className="box">
        {
          !this.props.isDefault ? (
            <div className="div-buttom-delete">
              <input className="delete-column" type="button" defaultValue="X" onClick={this.deleteColumn}></input>
            </div>)
          : ''
        }
        <div className={!this.props.isDefault ? "div-title-box" : ''}>
          <h2 className="h2-title-column">
            <div className="titleColumn">
              {this.props.columnTitle}
            </div>
          </h2>
        </div>
        {
          this.state.tasks.map(
              task => <Task key={task.pk} taskId={task.pk} taskDescription={task.description} taskTitle={task.title}></Task>
          )
        }
      </div>
    );
  }
}

export default Box;
