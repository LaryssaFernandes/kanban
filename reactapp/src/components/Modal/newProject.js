import React, {Component} from 'react';
import './modal.css';
import ProjectsService from "../../services/ProjectsService";
const  projectsService = new ProjectsService();

class newProgram extends Component {

    saveProgram = () => {
        const project = {
            "title": document.getElementById(this.props.children.props.id).value
        };
        if (project.title !== null) {
            projectsService.createProject(project).then(()=> {
                this.props.close();
            });
        }
    }

    render() {
        return (
            <div>
                <div className="modal-wrapper"
                    style={{
                        transform: this.props.show ? 'translateY(0vh)' : 'translateY(-100vh)',
                        opacity: this.props.show ? '1' : '0'
                    }}>
                    <div className="modal-header">
                        <h3>New Project</h3>
                        <span className="close-modal-btn" onClick={this.props.close}>×</span>
                    </div>
                    <div className="modal-body">
                        <p>
                            {this.props.children}
                        </p>
                    </div>
                    <div className="modal-footer">
                        <button className="btn-cancel" onClick={this.props.close}>CLOSE</button>
                        <button className="btn-continue" onClick={this.saveProgram}>SAVE</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default newProgram;