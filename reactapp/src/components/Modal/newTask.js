import React, {Component} from 'react';
import './modal.css';
import TasksService from "../../services/TasksService";
const  tasksService = new TasksService();

class newTask extends Component {

    saveProgram = () => {
        const task = {
            "title": document.getElementById('title-task').value,
            "description": document.getElementById('description-task').value,
            "project": document.getElementById('select-project').value || 1,
            "column": 1 
        };
        if (task.title !== null && task.description !== null) {
            tasksService.createTask(task).then(()=> {
                this.props.close();
            });
        }
    }

    render() {
        return (
            <div>
                <div className="modal-wrapper"
                    style={{
                        transform: this.props.show ? 'translateY(0vh)' : 'translateY(-100vh)',
                        opacity: this.props.show ? '1' : '0'
                    }}>
                    <div className="modal-header">
                        <h3>New Task</h3>
                        <span className="close-modal-btn" onClick={this.props.close}>×</span>
                    </div>
                    <div className="modal-body">
                        <p>
                            {this.props.children}
                        </p>
                    </div>
                    <div className="modal-footer">
                        <button className="btn-cancel" onClick={this.props.close}>CLOSE</button>
                        <button className="btn-continue" onClick={this.saveProgram}>SAVE</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default newTask;