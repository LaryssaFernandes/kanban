import React, {Component} from 'react';
import './modal.css';
import TasksService from "../../services/TasksService";
const  tasksService = new TasksService();

class editTask extends Component {

    updateTask = () => {
        const task = {
            "id": document.getElementById('id-edit-task').value,
            "title": document.getElementById('title-edit-task').value,
            "description": document.getElementById('description-edit-task').value,
            "project": document.getElementById('select-project').value || 1,
            "column": document.getElementById('select-column').value 
        };
        if (task.title !== null && task.description !== null) {
            tasksService.updateTask(task).then(()=> {
                this.props.close();
            });
        }
    }

    render() {
        return (
            <div>
                <div id="id-edit-task"></div>
                <div className="modal-wrapper">
                    <div className="modal-header">
                        <h3>Update Task</h3>
                        <span className="close-modal-btn" onClick={this.props.close}>×</span>
                    </div>
                    <div className="modal-body">
                        {this.props.children}
                    </div>
                    <div className="modal-footer">
                        <button className="btn-cancel" onClick={this.props.close}>CLOSE</button>
                        <button className="btn-continue" onClick={this.updateTask}>SAVE</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default editTask;