import React, {Component} from 'react';
import './modal.css';
import ColumnsService from "../../services/ColumnsService";
const  columnsService = new ColumnsService();

class newColumn extends Component {

    saveColumn = () => {
        const idTitle = this.props.children.props.children[0].props.id;
        const column = {
            "title": document.getElementById(idTitle).value,
            "isDefault": false,
            "project": document.getElementById('select-project').value || 1,
            "color": document.getElementById("cores").value
        };
        if (column.title !== null && column.project !== null) {
            columnsService.createColumn(column).then(()=> {
                this.props.close();
            });
        }
    }

    render() {
        return (
            <div className="modal-wrapper"
                style={{
                    transform: this.props.show ? 'translateY(0vh)' : 'translateY(-100vh)',
                    opacity: this.props.show ? '1' : '0'
                }}>
                <div className="modal-header">
                    <h3>New Column</h3>
                    <span className="close-modal-btn" onClick={this.props.close}>×</span>
                </div>
                <div className="modal-body">
                    <div className="padding">
                        {this.props.children}
                    </div>
                </div>
                <div className="modal-footer">
                    <button className="btn-cancel" onClick={this.props.close}>CLOSE</button>
                    <button className="btn-continue" onClick={this.saveColumn}>SAVE</button>
                </div>
            </div>
        )
    }
}

export default newColumn;