import React, { Component } from "react";
import "./Task.css";

class Task extends Component {

  constructor(props) {
    super(props);

    this.openTask = this.openTask.bind(this);

  }

  openTask() {
    document.getElementById("id-edit-task").value = this.props.taskId;
    document.getElementById("title-edit-task").value = this.props.taskTitle;
    document.getElementById("description-edit-task").value = this.props.taskDescription;
    document.getElementById("div-edit-task").style.display = "block";
  }

  render() {
    return (    
      <div className="task" onClick={this.openTask}>
        <h2>  
          {this.props.taskTitle}
        </h2>
      </div>
    );
  }
}

export default Task;
