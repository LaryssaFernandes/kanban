import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class ColumnsService{

    getColumns() {
        const url = `${API_URL}/api/columns/`;
        return axios.get(url).then(response => response.data);
    }
    getColumnsByURL(link){
        const url = `${API_URL}${link}`;
        return axios.get(url).then(response => response.data);
    }
    getColumn(id) {
        const url = `${API_URL}/api/column/${id}`;
        return axios.get(url).then(response => response.data);
    }
    deleteColumn(columnId){
        const url = `${API_URL}/api/column/${columnId}`;
        return axios.delete(url);
    }
    createColumn(column){
        const url = `${API_URL}/api/columns/`;
        return axios.post(url,column);
    }
    updateColumn(column){
        const url = `${API_URL}/api/column/${column.id}`;
        return axios.put(url,column);
    }
}

