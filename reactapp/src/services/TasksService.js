import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class TasksService{

    getTasks() {
        const url = `${API_URL}/api/tasks/`;
        return axios.get(url).then(response => response.data);
    }
    getTasksByURL(link){
        const url = `${API_URL}${link}`;
        return axios.get(url).then(response => response.data);
    }
    getTask(id) {
        const url = `${API_URL}/api/task/${id}`;
        return axios.get(url).then(response => response.data);
    }
    deleteTask(task){
        const url = `${API_URL}/api/task/${task.id}`;
        return axios.delete(url);
    }
    createTask(task){
        const url = `${API_URL}/api/tasks/`;
        return axios.post(url,task);
    }
    updateTask(task){
        const url = `${API_URL}/api/task/${task.id}`;
        return axios.put(url,task);
    }
}
