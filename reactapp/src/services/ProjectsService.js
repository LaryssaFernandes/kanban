import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class ProjectsService{

    getProjects() {
        const url = `${API_URL}/api/projects/`;
        return axios.get(url).then(response => response.data);
    }
    getProjectsByURL(link){
        const url = `${API_URL}${link}`;
        return axios.get(url).then(response => response.data);
    }
    getProject(id) {
        const url = `${API_URL}/api/project/${id}`;
        return axios.get(url).then(response => response.data);
    }
    deleteProject(project){
        const url = `${API_URL}/api/project/${project.id}`;
        return axios.delete(url);
    }
    createProject(project){
        const url = `${API_URL}/api/projects/`;
        return axios.post(url,project);
    }
    updateProject(project){
        const url = `${API_URL}/api/project/${project.id}`;
        return axios.put(url,project);
    }
}